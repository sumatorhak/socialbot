<?php

require_once 'libs/SocialBot.php';

$sb = SocialBot::getInstance();

$settings = $sb->getSettings();
$enabledAccTypes = json_decode($settings['account_type'], true);

// Check user count
$userCount = $settings['user_count'];
foreach ($enabledAccTypes as $type)
{
    $users = $sb->getAccountsByType($type);
    $signUpTasksCount = count($sb->getTasksByCriteria([
        'method = "signUp"',
        'status != "' . SocialBot::TASK_STATUS_ERROR . '"',
    ]));

    $diff = (count($users) + $signUpTasksCount) - $userCount;

    if ($diff < 0)
    {
        for ($i = 0; $i < abs($diff); $i++)
        {
            $userData = $sb->getRandUser();
            unset(
                $userData['rand_user_id'],
                $userData['middle_name'],
                $userData['building'],
                $userData['street'],
                $userData['city'],
                $userData['state'],
                $userData['username'],
                $userData['email'],
                $userData['phone'],
                $userData['cell'],
                $userData['picture_large'],
                $userData['picture_medium'],
                $userData['picture_thumbnail'],
                $userData['last_use']
            );

            $userData['birthday'] = date('m.d.Y', strtotime($userData['birthday']));

            $sb->createTask([
                'method' => 'signUp',
                'parameters' => json_encode($userData),
                'status' => SocialBot::TASK_STATUS_NEW
            ]);
        }
    }
}