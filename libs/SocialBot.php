<?php

ini_set('display_errors', true);

use SocialBot\Api\VKApi;
use SocialBot\Api\SMSRegApi;

class SocialBot
{
    /** @var \PDO */
    private $db;

    private $vk;

    private $client_id = 6168146;

    private $client_secret = 'Hx5bvvC4nIvxrmQumbOV';

    private $smsReg;

    private $smsRegApiKey = 'r5y9xd35iscqwkv4bb26tjqe9j6xidxk';

    private $isCLI = false;

    private $randUserApiUrl = 'http://randomuser.ru/api.json';

    private $settings;

    const TASK_STATUS_NEW = 'new';
    const TASK_STATUS_PROCESS = 'process';
    const TASK_STATUS_ERROR = 'error';

    public static function getInstance()
    {
        return new self;
    }

    public function __construct()
    {
        require_once __DIR__ . '/../api/VKApi.php';
        require_once __DIR__ . '/../api/SMSRegApi.php';

        $this->vk = new VKApi($this->client_id, $this->client_secret);
        $this->vk->setApiVersion(5.68);

        $this->smsReg = new SMSRegApi($this->smsRegApiKey);
        $this->smsReg->setRate(0);

        if ('cli' == PHP_SAPI)
        {
            $this->isCLI = true;
        }

        $this->initDB();

        $this->settings = $this->db->query("SELECT * FROM settings")->fetchAll(PDO::FETCH_ASSOC);
        $this->prepareSettings();
    }

    private function prepareSettings()
    {
        $prepared = [];
        foreach ($this->settings as $setting)
        {
            $prepared[$setting['name']] = $setting['value'];
        }

        $this->settings = $prepared;
    }

    public function getSettings($name = null)
    {

        return $this->settings;
    }

    public function setSettings()
    {
        // not implemented
    }

    private function initDB()
    {
        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        ];
        $this->db = new \PDO("mysql:host=10.241.8.12;dbname=social", 'root', 'YaF5AAMhm9', $options);
    }

    public function taskHandler()
    {
        $task = $this->db->query("SELECT * FROM tasks WHERE status IN ('" . self::TASK_STATUS_NEW . "', '" . self::TASK_STATUS_PROCESS . "')")->fetch(PDO::FETCH_ASSOC);

        $response = $this->{$task['method']}($task);

        if(isset($response['error']))
        {
            $this->errorHandler($response['error']);
        }
    }

    public function signUp($task)
    {
        $taskId = $task['task_id'];
        $parameters = json_decode($task['parameters'], true);

        $response = null;

        if ($task['status'] == self::TASK_STATUS_NEW)
        {
            /** Check balance for sms activation  */
            if (false === $this->smsReg->isGoodBalance())
            {
                return false;
            }

            //$parameters['tzid'] = $this->smsReg->getNum();
            //$this->regVK($parameters);

            $this->db->query("UPDATE tasks SET status = '" . self::TASK_STATUS_PROCESS . "' WHERE task_id = " . $taskId);
        }
        else
        {
            $response = $this->regVK($parameters);
        }

        return $response;
    }

    private function regVK($parameters)
    {
        $tzid = $parameters['tzid'];

        $state = NULL;

        if (is_int($tzid))
        {
            $state = $this->smsReg->getState($tzid);
        }

        $parameters['tzid'] = $tzid;

        $response = NULL;
        if (
            isset($state['response']) && $state['response'] == 'TZ_NUM_PREPARE' &&
            isset($state['number']) && isset($state['service']) &&
            $state['service'] == $this->smsReg->getService()
        )
        {
            $state['number'] = '+79857189932'; //remove

            $checkData = $this->vk->checkPhone($state['number']);

            /** if phone number is used by another user */
            if (isset($checkData['error']))
            {
                $this->smsReg->setOperationUsed($tzid);

                $this->log('Phone used');

                return false;
            }

            $parameters['test_mode'] = 1; // no reg user and no use phone number

            $parameters['phone'] = $state['number'];


            $response = $this->vk->api($this->vk::VK_METHOD_SIGN_UP, $parameters);

            return $response;

            $this->smsReg->setReady($tzid);
        }
        else
        {
            $this->smsReg->responseHandler($response);
        }
    }

    public function setOnline()
    {

    }

    private function errorHandler($error)
    {
        if (isset($error['error_code']))
        {
            switch ($error['error_code'])
            {
                case VKApi::ERROR_TOO_MANY_EQ_ACTIONS:
                {
                    throw new \Exception(isset($error['error_text'])?$error['error_text']:$error['error_msg']); break;
                }
                case VKApi::ERROR_CAPTCHA_NEEDED:
                {
                    $this->log('Need captcha! Wait 60 second...');

                    $captchaSig = $error['captcha_sid'];
                    $captchaImg = $error['captcha_img'];


                    break;
                }
                default:
                {
                    throw new \Exception(isset($error['error_text'])?$error['error_text']:$error['error_msg']); break;
                }
            }
        }
    }

    private function log($text)
    {
        echo "\n$text\n";
    }

    private function getRandomUser()
    {
        $data = file_get_contents($this->randUserApiUrl);

        if (!empty($data))
        {
            $data = json_decode($data, true);
            $data = array_pop($data)['user'];
        }
        else
        {
            return false;
        }

        return $data;
    }

    public function collectRandUser()
    {
        $user = $this->getRandomUser();

        // Generate timestamp between 1970-01-01 and 1999-12-31. Range 30 years
        $randTimestamp = rand(0, 946598400);
        $user['birthday'] = date('Y-m-d', $randTimestamp);

        $user['gender'] = intval($user['gender'] == 'male');

        $user['first_name'] = $user['name']['first'];
        $user['last_name'] = $user['name']['last'];
        $user['middle_name'] = $user['name']['middle'];
        unset($user['name']);

        $user['building'] = $user['location']['building'];
        $user['street'] = $user['location']['street'];
        $user['city'] = $user['location']['city'];
        $user['state'] = $user['location']['state'];
        $user['zip'] = $user['location']['zip'];
        unset($user['location']);

        $user['picture_large'] = $user['picture']['large'];
        $user['picture_medium'] = $user['picture']['medium'];
        $user['picture_thumbnail'] = $user['picture']['thumbnail'];
        unset($user['picture']);

        $randUserId = md5(serialize($user));
        $user['rand_user_id'] = $randUserId;

        //$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING ); //test

        $stmt = $this->db->prepare("
            INSERT INTO rand_users (
                rand_user_id,
                gender,
                first_name,
                last_name,
                middle_name,
                birthday,
                building,
                street,
                city,
                state,
                username,
                email,
                password,
                phone,
                cell,
                picture_large,
                picture_medium,
                picture_thumbnail
            ) VALUES (
                :rand_user_id,
                :gender,
                :first_name, 
                :last_name,
                :middle_name,
                :birthday,
                :building,
                :street,
                :city,
                :state,
                :username,
                :email,
                :password,
                :phone,
                :cell,
                :picture_large,
                :picture_medium,
                :picture_thumbnail
            )
        ");

        if (!$stmt->execute($user))
        {
            return false;
        }

        return $user;
    }

    public function getAccount()
    {
        return $this->db->query("SELECT * FROM accounts WHERE `type` = 'vk' ORDER BY last_activity ASC")->fetch(PDO::FETCH_ASSOC);
    }

    public function getAccountsByType($type)
    {
        return $this->db->query("SELECT * FROM accounts WHERE `type` = '$type'")->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTasksByCriteria($criteria)
    {
        return $this->db->query("SELECT * FROM `tasks` WHERE " . implode(" AND ", $criteria))->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createTask($data)
    {
        $sql = "INSERT INTO tasks (method, parameters, status) VALUES (:method, :parameters, :status)";
        $stmt = $this->db->prepare($sql);

        if (!$stmt->execute($data))
        {
            return false;
        }

        return true;
    }

    public function getRandUser()
    {
        $user = $this->db->query("SELECT * FROM rand_users ORDER BY last_use ASC LIMIT 1")->fetch(PDO::FETCH_ASSOC);

        $stmt = $this->db->prepare("UPDATE rand_users SET last_use = NOW() WHERE rand_user_id = :rand_user_id");

        if(!$stmt->execute(['rand_user_id' => $user['rand_user_id']]))
        {
            return false;
        }

        return $user;
    }

    public function singIn($parameters)
    {
        $response = file_get_contents($this->vk->getAuthorizeUrl());

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($response);

        $form = $doc->getElementsByTagName('form')->item(0);

        $postURL = $form->getAttribute('action');
        $postURL = 'https://login.vk.com/?act=login&soft=1';
        $postData = [];

        $inputs = $form->getElementsByTagName("input");

        for ($i = 0; $i < $inputs->length; $i++)
        {
            if ($inputs->item($i)->getAttribute('name'))
            {
                $postData[$inputs->item($i)->getAttribute('name')] = $inputs->item($i)->getAttribute('value');
            }
        }

        $postData['email'] = '89636616060';
        $postData['pass'] = '11109091sum';
        $postData['expire'] = '0';

        $response = $this->request($postURL, 'POST', $postData);

        echo $postURL . "\n";
        print_r(mb_convert_encoding($response, 'utf-8', 'cp1251'));
    }

    private function request($url, $method = 'GET', $postfields = [])
    {
        $ch = curl_init();

        $tmpfname = dirname(__FILE__).'/../cookie.txt';

        curl_setopt_array($ch, [
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HEADER => 1,
            CURLOPT_POST           => ($method == 'POST'),
            CURLOPT_POSTFIELDS     => http_build_query($postfields),
            CURLOPT_URL            => $url,
            CURLOPT_COOKIEJAR      => $tmpfname,
            CURLOPT_COOKIEFILE     => $tmpfname,
            CURLOPT_HTTPHEADER     => [
                'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                'Cache-control:no-cache',
                'Content-type:application/x-www-form-urlencoded',
                'Origin:https://oauth.vk.com',
                'upgrade-insecure-requests:1',
                'Referer: https://oauth.vk.com/oauth/authorize?client_id=6168146&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&response_type=token',
            ]
        ]);

        return curl_exec($ch);
    }
}