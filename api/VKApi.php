<?php

namespace SocialBot\Api;

class VKApi
{
    /**
     * VK application id.
     * @var string
     */
    private $client_id;
    /**
     * VK application secret key.
     * @var string
     */
    private $client_secret;
    /**
     * API version. If null uses latest version.
     * @var int
     */
    private $api_version;
    /**
     * VK access token.
     * @var string
     */
    private $access_token;
    /**
     * Authorization status.
     * @var bool
     */
    private $auth = false;
    /**
     * Instance curl.
     * @var Resource
     */
    private $ch;

    const AUTHORIZE_URL = 'https://oauth.vk.com/authorize';
    const ACCESS_TOKEN_URL = 'https://oauth.vk.com/access_token';

    const ERROR_UNKNOWN = 1;
    const ERROR_APP_DISABLED = 2;
    const ERROR_UNKNOWN_METHOD = 3;
    const ERROR_WRONG_SIGN = 4;
    const ERROR_USER_AUTH_FAILED = 5;
    const ERROR_TOO_MANY_REQUESTS = 6;
    const ERROR_ACTION_NOT_PERMITTED = 7;
    const ERROR_WRONG_REQUEST = 8;
    const ERROR_TOO_MANY_EQ_ACTIONS = 9;
    const ERROR_INTERNAL_SERVER = 10;
    const ERROR_IN_TEST_APP_OFF = 11;
    const ERROR_CAPTCHA_NEEDED = 14;
    const ERROR_ACCESS_DENIED = 15;
    const ERROR_NEED_HTTPS = 16;
    const ERROR_NEED_USER_VALIDATION = 17;
    const ERROR_PAGE_DEL_OR_LOCKED = 18;
    const ERROR_ACTION_DISALLOW_STANDALONE = 20;
    const ERROR_ACTION_ALLOW_STANDALONE_OPEN_API = 21;
    const ERROR_METHOD_HAS_BEEN_DISALLOW = 23;
    const ERROR_NEED_USER_CONFIRM = 24;
    const ERROR_ACCESS_KEY_GROUP_WRONG = 27;
    const ERROR_ACCESS_KEY_APP_WRONG = 28;
    const ERROR_WRONG_REQUIRED_PARAMS = 100;
    const ERROR_WRONG_API_ID = 101;
    const ERROR_WRONG_USER_ID = 113;
    const ERROR_WRONG_TIMESTAMP = 150;
    const ERROR_ACCESS_DENIED_TO_ALBUM = 200;
    const ERROR_ACCESS_DENIED_TO_AUDIO = 201;
    const ERROR_ACCESS_DENIED_TO_GROUP = 203;
    const ERROR_ALBUM_IF_FULL = 300;
    const ERROR_VOICE_TRANSACTION_DISABLED = 500;
    const ERROR_AD_NOT_PERMITTED = 600;
    const ERROR_INTERNAL_IN_AD = 603;

    const ERROR_INCORRECT_PHONE = 1000;
    const ERROR_PHONE_ALREADY_EXISTS = 1004;
    const ERROR_PREPARED_TRY_AGAIN = 1112;




    const VK_METHOD_SIGN_UP = 'auth.signup';
    const VK_METHOD_CHECK_PHONE = 'auth.checkPhone';

    /**
     * Constructor.
     * @param   string $client_id
     * @param   string $client_secret
     * @param   string $access_token
     */
    public function __construct($client_id, $client_secret, $access_token = NULL)
    {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->setAccessToken($access_token);
        $this->ch = curl_init();
    }

    /**
     * Destructor.
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * Set special API version.
     * @param   int $version
     * @return  void
     */
    public function setApiVersion($version)
    {
        $this->api_version = $version;
    }

    /**
     * Set Access Token.
     * @param   string $access_token
     * @return  void
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }

    /**
     * Returns base API url.
     * @param   string $method
     * @param   string $response_format
     * @return  string
     */
    public function getApiUrl($method, $response_format = 'json')
    {
        return 'https://api.vk.com/method/' . $method . '.' . $response_format;
    }

    /**
     * Returns authorization link with passed parameters.
     * @param   string $api_settings
     * @param   string $callback_url
     * @param   bool $test_mode
     * @return  string
     */
    public function getAuthorizeUrl($api_settings = '', $callback_url = 'https://api.vk.com/blank.html', $test_mode = false)
    {
        $parameters = [
            'client_id'     => $this->client_id,
            'scope'         => $api_settings,
            'redirect_uri'  => $callback_url,
            'response_type' => 'code'
        ];
        if ($test_mode)
        {
            $parameters['test_mode'] = 1;
        }

        return $this->createUrl(self::AUTHORIZE_URL, $parameters);
    }

    /**
     * Returns access token by code received on authorization link.
     * @param $code
     * @param string $callback_url
     * @return mixed
     * @throws \Exception
     */
    public function getAccessToken($code, $callback_url = 'https://api.vk.com/blank.html')
    {
        if (!is_null($this->access_token) && $this->auth)
        {
            throw new \Exception('Already authorized.');
        }

        $parameters = [
            'client_id'     => $this->client_id,
            'client_secret' => $this->client_secret,
            'code'          => $code,
            'redirect_uri'  => $callback_url
        ];
        $rs = json_decode($this->request($this->createUrl(self::ACCESS_TOKEN_URL, $parameters)), true);

        if (isset($rs['error']))
        {
            throw new \Exception($rs['error'] . (!isset($rs['error_description']) ?: ': ' . $rs['error_description']));
        }
        else
        {
            $this->auth = true;
            $this->access_token = $rs['access_token'];

            return $rs;
        }
    }

    /**
     * Return user authorization status.
     * @return  bool
     */
    public function isAuth()
    {
        return !is_null($this->access_token);
    }

    /**
     * Check for validity access token.
     * @param   string $access_token
     * @return  bool
     */
    public function checkAccessToken($access_token = NULL)
    {
        $token = is_null($access_token) ? $this->access_token : $access_token;
        if (is_null($token))
        {
            return false;
        }
        $rs = $this->api('getUserSettings', ['access_token' => $token]);

        return isset($rs['response']);
    }

    /**
     * Execute API method with parameters and return result.
     * @param   string $method
     * @param   array $parameters
     * @param   string $format
     * @param   string $requestMethod
     * @return  mixed
     */
    public function api($method, $parameters = [], $format = 'array', $requestMethod = 'get')
    {
        $parameters['client_id'] = $this->client_id;
        $parameters['client_secret'] = $this->client_secret;

        if (!array_key_exists('access_token', $parameters) && !is_null($this->access_token))
        {
            $parameters['access_token'] = $this->access_token;
        }

        if (!array_key_exists('v', $parameters) && !is_null($this->api_version))
        {
            $parameters['v'] = $this->api_version;
        }

        if ($method == 'execute' || $requestMethod == 'post')
        {
            $rs = $this->request($this->getApiUrl($method, $format == 'array' ? 'json' : $format), "POST", $parameters);
        }
        else
        {
            $rs = $this->request($this->createUrl($this->getApiUrl($method, $format == 'array' ? 'json' : $format), $parameters));
        }

        return $format == 'array' ? json_decode($rs, true) : $rs;
    }

    /**
     * Concatenate keys and values to url format and return url.
     * @param   string $url
     * @param   array $parameters
     * @return  string
     */
    private function createUrl($url, $parameters)
    {
        $url .= '?' . http_build_query($parameters);

        return $url;
    }

    /**
     * Executes request on link.
     * @param   string $url
     * @param   string $method
     * @param   array $postfields
     * @return  string
     */
    private function request($url, $method = 'GET', $postfields = [])
    {
        curl_setopt_array($this->ch, [
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST           => ($method == 'POST'),
            CURLOPT_POSTFIELDS     => $postfields,
            CURLOPT_URL            => $url
        ]);

        return curl_exec($this->ch);
    }

    public function checkPhone($phone, $authByPhone = 0)
    {
        $response = $this->api(self::VK_METHOD_CHECK_PHONE, [
            'phone'         => $phone,
            'auth_by_phone' => $authByPhone,
        ]);

        if (isset($response['response']) && $response['response'] == 1)
        {
            return $response['response'];
        }

        return $response;
    }
}