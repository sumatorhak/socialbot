<?php

namespace SocialBot\Api;

class SMSRegApi
{
    private $api_key;

    private $service = 'vk';

    private $country = 'ru';

    /**
     * Instance curl.
     * @var Resource
     */
    private $ch;

    private $rate = 0;

    const OPERATION_ACTIVE = 'active';
    const OPERATION_COMPLETED = 'completed';


    /** операция ожидает выделения номера */
    const TZ_INPOOL = 'TZ_INPOOL';
    /** выдан номер, ожидается выполнение метода SetReady */
    const TZ_NUM_PREPARE = 'TZ_NUM_PREPARE';
    /** ожидается ответ */
    const TZ_NUM_WAIT = 'TZ_NUM_WAIT';
    /** поступил ответ */
    const TZ_NUM_ANSWER = 'TZ_NUM_ANSWER';
    /** ожидается уточнение полученного кода */
    const TZ_NUM_WAIT2 = 'TZ_NUM_WAIT2';
    /** поступил ответ после уточнения */
    const TZ_NUM_ANSWER2 = 'TZ_NUM_ANSWER2';
    /** нету подходящих номеров */
    const WARNING_NO_NUMS = 'WARNING_NO_NUMS';
    /**
     * Также если время по операции уже истекло то получите следующие значения:
     */
    /** операция завершена */
    const TZ_OVER_OK = 'TZ_OVER_OK';
    /** операция отмечена как ошибочная */
    const TZ_OVER_GR = 'TZ_OVER_GR';
    /** ответ не поступил за отведенное время */
    const TZ_OVER_EMPTY = 'TZ_OVER_EMPTY';
    /** вы не отправили запрос методом setReady */
    const TZ_OVER_NR = 'TZ_OVER_NR';
    /** уточнение не поступило за отведенное время */
    const TZ_OVER2_EMPTY = 'TZ_OVER2_EMPTY';
    /** операция завершена после уточнения */
    const TZ_OVER2_OK = 'TZ_OVER2_OK';
    /** операция удалена, средства возвращены */
    const TZ_DELETED = 'TZ_DELETED';

    /**
     * SMSRegApi constructor.
     * @param $api_key
     */
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
        $this->ch = curl_init();
    }

    /**
     * SMSRegApi destructor
     */
    public function __destruct()
    {
        curl_close($this->ch);
    }

    /**
     * Returns base API url.
     * @param   string $method
     * @return  string
     */
    public function getApiUrl($method)
    {
        return 'http://api.sms-reg.com/' . $method . '.php';
    }

    /**
     * Execute API method with parameters and return result.
     * @param   string $method
     * @param   array $parameters
     * @param   string $format
     * @return  mixed
     */
    public function api($method, $parameters = [], $format = 'array')
    {
        $defaultParameters = [
            'apikey' => $this->api_key,
            'service' => $this->service,
        ];
        $parameters = array_merge($defaultParameters, $parameters);

        $rs = $this->request($this->getApiUrl($method, $format == 'array' ? 'json' : $format), "POST", $parameters);

        return $format == 'array' ? json_decode($rs, true) : $rs;
    }

    /**
     * Executes request on link.
     * @param   string $url
     * @param   string $method
     * @param   array $postfields
     * @return  string
     */
    private function request($url, $method = 'GET', $postfields = [])
    {
        curl_setopt_array($this->ch, [
            CURLOPT_USERAGENT      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POST           => ($method == 'POST'),
            CURLOPT_POSTFIELDS     => $postfields,
            CURLOPT_URL            => $url
        ]);

        return curl_exec($this->ch);
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setRate($rate)
    {
        $this->rate = $rate;
        return $this->api('setRate', ['rate' => $rate]);
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Check balance for SMS activation
     * @return bool
     */
    public function isGoodBalance()
    {
        $currentBalance = $this->api('getBalance');
        $currentBalance = floatval(isset($currentBalance['balance']) ? $currentBalance['balance'] : 0);
        $amountActivation = 12.0;
        $totalAmount = $amountActivation + $this->getRate();

        return ($totalAmount <= $currentBalance);
    }

    /**
     * Return list of operations with state
     * @param $opstate
     * @param int $count default = 100, max = 1000
     * @param string $output 'array' or 'object'
     * @return mixed
     */
    public function getOperations($opstate, $count = 100, $output = 'array')
    {
        return $this->api('getOperations', [
            'opstate'   => $opstate,
            'count'     => intval($count),
            'output'    => $output
        ]);
    }

    public function getNum($country = null, $service = null, $appId = null)
    {
        $country = is_null($country) ? $this->country : $country;
        $service = is_null($service) ? $this->service : $service;

        $response = $this->api('getNum', [
            'country' => $country,
            'service' => $service,
            'appid' => $appId
        ]);

        if (isset($response['tzid']))
        {
            return intval($response['tzid']);
        }

        return $response;
    }

    public function getState($tzid)
    {
        return $this->api('getState', ['tzid' => intval($tzid)]);
    }

    public function setReady($tzid)
    {
        return $this->api('setReady', ['tzid' => intval($tzid)]);
    }

    public function setOperationUsed($tzid)
    {
        return $this->api('setOperationUsed', ['tzid' => intval($tzid)]);
    }

    public function responseHandler($response)
    {
        $response = $response['response'];

        switch ($response)
        {
            case self::TZ_INPOOL:
            {
                break;
            }
            case self::TZ_NUM_PREPARE:
            {
                break;
            }
            case self::TZ_NUM_WAIT:
            {
                break;
            }
            case self::TZ_NUM_ANSWER:
            {
                break;
            }
            case self::TZ_NUM_WAIT2:
            {
                break;
            }
            case self::TZ_NUM_ANSWER2:
            {
                break;
            }
            case self::WARNING_NO_NUMS:
            {
                break;
            }
            case self::TZ_OVER_OK:
            {
                break;
            }
            case self::TZ_OVER_GR:
            {
                break;
            }
            case self::TZ_OVER_EMPTY:
            {
                break;
            }
            case self::TZ_OVER_NR:
            {
                break;
            }
            case self::TZ_OVER2_EMPTY:
            {
                break;
            }
            case self::TZ_OVER2_OK:
            {
                break;
            }
            case self::TZ_DELETED:
            {
                break;
            }
            default:
            {
                break;
            }
        }
    }
}