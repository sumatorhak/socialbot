<?php

class VK
{
    //https://oauth.vk.com/authorize?client_id=6168146&display=page&redirect_uri=https://oauth.vk.com/blank.html&response_type=token&v=5.68

    const OAUTH = 'https://oauth.vk.com/authorize';

    const CLIENT_ID = 6168146;

    const REDIRECT_URL = 'https://oauth.vk.com/blank.html';

    private $session;

    public function login($login, $password)
    {
        // cookie (remixlang, remixlhk)
        $response = $this->request(self::OAUTH, [
            'client_id' => self::CLIENT_ID,
            'display' => 'page',
            'redirect_uri' => 'https://oauth.vk.com/blank.html',
            'response_type' => 'token',
            'v' => '5.68',
            'scope' => 'status, friends'
        ]);

        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($response);

        $form = $doc->getElementsByTagName('form')->item(0);

        $postURL = $form->getAttribute('action');
        $postData = [];

        $inputs = $form->getElementsByTagName("input");

        for ($i = 0; $i < $inputs->length; $i++)
        {
            if ($inputs->item($i)->getAttribute('name'))
            {
                $postData[$inputs->item($i)->getAttribute('name')] = $inputs->item($i)->getAttribute('value');
            }
        }

        $postData['email'] = $login;
        $postData['pass'] = $password;

        $res = $this->request($postURL, $postData, 'POST', 1);

        preg_match('/"(https:\/\/login\.vk\.com\/\?act=grant_access&client_id=' . self::CLIENT_ID . '[^"]+)"/i', $res, $matches);

        if (isset($matches[1]))
        {
            $res = $this->request($matches[1], NULL, 'GET', 1);
        }

        preg_match('/Location: https:\/\/oauth.vk.com\/blank.html#(.*)/i', $res, $matches2);

        $data = []; $preparedData = [];
        if (isset($matches2[1]))
        {
            $data = explode('&', $matches2[1]);
        }

        foreach ($data as $row)
        {
            $parts = explode('=', $row);
            $preparedData[$parts[0]] = $parts[1];
        }

        //save in DB (access_token, expires_in, user_in, state)
        //print_r($preparedData);

        $ddd = $this->request('https://api.vk.com/method/status.set.json', [
            'access_token' => $preparedData['access_token'],
            'text' => 'Ищу идеи для творчества...'
        ]);

        echo $ddd;
    }

    protected function request($url, $params = [], $method = 'POST', $header = 0)
    {
        $ch = curl_init($url);

        curl_setopt_array($ch, [
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => intval($method == 'POST'),
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_COOKIEFILE => __DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt',
            CURLOPT_COOKIEJAR => __DIR__ . DIRECTORY_SEPARATOR . 'cookie.txt',
            CURLOPT_HTTPHEADER => [
                'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'accept-language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
                'cache-control: no-cache',
                'pragma: no-cache',
                'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.113 Chrome/60.0.3112.113 Safari/537.36',
            ],
            CURLOPT_HEADER => $header
        ]);

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    public function showMe()
    {
        $res = $this->request('https://vk.com', null, 'GET');

        $res = str_replace('setTimeout', '', $res);

        print_r($res);
    }

}

//header('Content-type: text/html; charset=cp1251');
$vk = new VK();

$vk->login('+79151589829', '859182qQ');

//$vk->showMe();

